class Book
  attr_accessor :title

  def initialize
    @title
  end

  def title

    bad_words = ["and", "in", "the", "an", "a", "of"]

    @title.split.map!.with_index do |word, idx|
      if !bad_words.include?(word) || idx == 0
        word.capitalize
      else
        word
      end
    end.join(" ")

  end
end
