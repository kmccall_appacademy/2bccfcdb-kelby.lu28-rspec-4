class Temperature

  def self.from_celsius(temp)
    self.new({c: temp})
  end

  def self.from_fahrenheit(temp)
    self.new({f: temp})
  end

  def self.ftoc(temp)
    (temp - 32) * 5/9
  end

  def self.ctof(temp)
    (temp * 9.0/5) + 32
  end

  attr_accessor :fahrenheit, :celsius

  def initialize(options)
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  def in_fahrenheit
    @fahrenheit ? @fahrenheit : self.class.ctof(@celsius)
  end

  def in_celsius
    @celsius ? @celsius : self.class.ftoc(@fahrenheit)
  end
end

class Celsius < Temperature

  def initialize(temp)
    @celsius = temp
  end

end

class Fahrenheit < Temperature

  def initialize(temp)
    @fahrenheit = temp
  end

end
