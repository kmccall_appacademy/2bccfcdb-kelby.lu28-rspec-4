class Dictionary

  attr_accessor :keywords, :entries

  def initialize
    @entries = {}
  end

  def add(words)
    if words.class == String
      @entries[words] = nil
    else
      @entries = @entries.merge(words)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(entry)
    keywords.include?(entry)
  end

  def find(entry)
    @entries.select do |key, value|
      key.include?(entry)
    end
  end

  def printable
    ary = []
    @entries.sort.each do |keyword, definition|
      ary << "[#{keyword}] \"#{definition}\""
    end

    ary.join("\n")
  end
end
